var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

/* GET - DEMO1 - render template */
router.get('/demo1',function (req, res, next) {
  res.render('demo1',
      {
        message: 'tes lorem ipsum',
        user:{
          name:'suyono',
          email:'suyono@example.com',
          website:'http://www.suyono.com'
        }
      });
});

/* GET - DEMO2 - parameter url */
router.get('/demo2/(:id)/(:category)',function (req, res, next) {
    res.render('demo2',
        {
           id : req.params.id,
           category : req.params.category
        });
});

/* GET - DEMO3 - menampilkan respone JSON */
router.get('/demo3', function (req, res, next) {
    res.json(
        {
            message: 'tes lorem ipsum',
            user:{
                name:'suyono',
                email:'suyono@example.com',
                website:'http://www.suyono.com'
            }
        });

});

/* GET - DEMO4 - menerima request method POST */
router.get('/demo4',function (req, res, next) {
    res.render('demo4');
});

router.post('/demo4',function (req, res, next) {
    res.json(
        {
            message: 'request POST is executed',
            user:{
                name:req.param('username'),
                email:req.param('email'),
                website:req.param('website'),
                phone:req.param('phone')
            }
    });

});

module.exports = router;
