var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

/* GET users child listing. */
router.get('/child', function(req, res, next) {
    res.send('respond with a resource of users child');
});

module.exports = router;
